panel2lite
----------

This is a stripped down version of the panel2 core framework for accessing the DNS
structures.

Original copyright applies.

To use, simply put a copy of your panel2.conf in the panel2lite directory.
