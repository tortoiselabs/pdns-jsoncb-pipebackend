#!/usr/bin/env python
"""
Copyright (c) 2012, 2013  TortoiseLabs LLC

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice, this permission notice and all necessary source code
to recompile the software are included or otherwise available in all
distributions.

This software is provided 'as is' and without any warranty, express or
implied.  In no event shall the authors be liable for any damages arising
from the use of this software.
"""

from panel2lite.dns.models import Domain, Record
import requests
import sys
import json

###################################################################################
# utility functions                                                               #
###################################################################################

dn_lookup_subset = lambda dname_subset: Record.query.filter_by(name=dname_subset).filter_by(type='JSONCB').first()

def dn_lookup(qname):
    dots = len(qname.split('.'))
    for i in xrange(0, dots):
        sp = qname.split('.', i)
        rec = dn_lookup_subset(sp[-1])
        if rec:
            return rec

def dn_json_req(parent, qname, qtype, remote_ip, local_ip, edns_subnet):
    message = {
        'qname': qname,
        'qtype': qtype,
        'remote_ip': remote_ip,
        'local_ip': local_ip,
        'edns_subnet': edns_subnet,
    }
    r = requests.post(parent.content, message, timeout=1.0)
    if r.status_code != 200:
        r = requests.get(parent.content, timeout=1.0)
    return json.loads(r.text)

###################################################################################
# command handlers                                                                #
###################################################################################

def write_coproc(cmd, args=[]):
    vec = [cmd] + args
    sys.stdout.write('\t'.join(vec) + '\n')
    sys.stdout.flush()

def write_answer(qname, type, id, content, scopebits=0, ttl=300):
    write_coproc('DATA', [str(scopebits), str(1), qname, 'IN', type, str(ttl), id, content])

def dump_record(r, fallback_qname, fallback_qtype, id):
    if not r.has_key('qname'):
        r['qname'] = fallback_qname
    if not r.has_key('qtype'):
        r['qtype'] = fallback_qname
    if not r.has_key('ttl'):
        r['ttl'] = 300
    write_answer(r['qname'], r['qtype'], id, r['content'], ttl=r['ttl'])

def hdl_HELO(args):
    if args[1] != '3':
        write_coproc('FAIL')
        exit()
    write_coproc('OK', ['JSONCB backend firing up'])

def hdl_PING(args):
    write_coproc('END')

def hdl_Q(args):
    qname = args[1]
    qclass = args[2]
    qtype = args[3]
    id = args[4]
    remote_ip = args[5]
    local_ip = args[6]
    edns_subnet = args[7]

    rec = dn_lookup(qname)
    if rec:
        try:
            st = dn_json_req(rec, qname, qtype, remote_ip, local_ip, edns_subnet)
        except:
            write_coproc('END')
            return

        if type(st) is list:
            [dump_record(r, qname, qtype, id) for r in st]
        else:
            dump_record(st, qname, qtype, id)

    write_coproc('END')

cmd_impl = {
    'HELO': hdl_HELO,
    'PING': hdl_PING,
    'AXFR': hdl_PING,
    'Q':    hdl_Q,
}

###################################################################################
# main program                                                                    #
###################################################################################

def io_loop():
    while True:
        line = sys.stdin.readline().strip('\n')
        data = line.split()

        if not cmd_impl.has_key(data[0]):
            write_coproc('FAIL')
            exit()

        try:
            cmd_impl[data[0]](data)
        except Exception as e:
            write_coproc('LOG', 'Exception: {}'.format(repr(e)))
            write_coproc('END')

if __name__ == '__main__':
    io_loop()
